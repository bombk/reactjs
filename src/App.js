import './App.css';
import Person from './Components/Person';
import Man from "./Components/Man"
import Demo from './Components/Demo';

function App() {
  return (
    <div className="App">
      <header className="App-header"> </header>
      <Demo number='1'></Demo>
       <h1>im a react app</h1>
      <Person name="ram"/>
      <Man/>
      <Demo number='2'></Demo>
       <h1>im a react app</h1>
      <Person name="ram"/>
      <Man/>
      <Demo number='3'></Demo>
       <h1>im a react app</h1>
      <Person name="ram"/>
      <Man/>
    </div>
  );
}

export default App;
